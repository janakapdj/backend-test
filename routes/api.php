<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('orders', 'OrdersController@index');
Route::get('orders/{order}', 'OrdersController@show');
Route::get('/orders/{id}','OrdersController@fetchOrderData');
Route::post('orders', 'OrdersController@store');
Route::put('orders/{order}', 'OrdersController@update');
Route::delete('orders/{order}', 'OrdersController@delete');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
