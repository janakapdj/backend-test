<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [];
    protected $hidden = [];

    public function orderproduct(){
        return $this->belongsTo('App\Models\Orderdetail','productCode','productCode');
    }
}
