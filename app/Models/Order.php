<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
  /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'orderDate', 'requiredDate', 'shippedDate', 'status', 'comments', 'customerNumber'
    ];
    protected $with = ['orderdetails','customer'];
    protected $primaryKey = 'orderNumber';

    public function customer(){
        return $this->belongsTo('App\Models\Customer','customerNumber','customerNumber');
    }

    public function orderdetails(){
        return $this->hasMany('App\Models\Orderdetail','orderNumber','orderNumber');
    }
}
