<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Orderdetail extends Model
{

    protected $fillable = [];
    protected $hidden = [];
    protected $primaryKey = 'orderNumber';
    protected $with = ['products'];

    public function order(){
        return $this->belongsTo('App\Models\Order','orderNumber','orderNumber');
    }

    public function products(){
        return $this->hasMany('App\Models\Product','productCode','productCode');
    }
}
