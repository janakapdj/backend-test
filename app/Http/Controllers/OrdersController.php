<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;

class OrdersController extends Controller
{
    public function index()
    {
        
    }

    public function show($id)
    {
        if(!Order::where('orderNumber',$id)->exists()){
            return response()->json(['error' => 'Sorry order not exists'], 400);
        }
        $orderdetail = Order::findOrFail($id);
        if(!empty($orderdetail)){
            $orderItems = [];
            $bill_amount = 0;
            if(!empty($orderdetail->orderdetails)){
                foreach($orderdetail->orderdetails as $order_itm){
                    $line_total = floatval($order_itm->priceEach) * intval($order_itm->quantityOrdered);
                    $line_total_format = number_format((float)$line_total, 2, '.', '');
                    $bill_amount = $bill_amount+ $line_total;
                    $details = [
                        "product"=>$order_itm->products[0]->productName,
                        "product_line"=>$order_itm->products[0]->productLine,
                        "unit_price"=>number_format((float)$order_itm->priceEach, 2, '.', ''),
                        "qty"=>intval($order_itm->quantityOrdered),
                        "line_total"=> $line_total_format,
                    ];
                    array_push($orderItems,$details);
                }
            }
            $oder_details = [
                "order_id" => $orderdetail->orderNumber,
                "order_date"=> $orderdetail->requiredDate,
                "status"=> $orderdetail->status,
                "order_details" => $orderItems,
                "bill_amount" => number_format((float)$bill_amount, 2, '.', ''),
                "customer" => [
                    "first_name"=>$orderdetail->customer->contactFirstName,
                    "last_name"=>$orderdetail->customer->contactLastName,
                    "phone"=>$orderdetail->customer->phone,
                    "country_code"=>$orderdetail->customer->postalCode,
                    ]
            ];
            return response()->json($oder_details, 200);
        }
    }

    public function store(Request $request)
    {
        
    }

    public function update(Request $request, $id)
    {
    
    }

    public function delete(Request $request, $id)
    {
 
    }
}
